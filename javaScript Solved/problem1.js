const calPoints = (ops) => {
  let result = [];
  for (const op of ops) {
    const si = result.length - 1;
    let newScore = 0;
    switch (op) {
      case "+":
        newScore = result[si] + result[si - 1]; //sum of previous 2 scores
        result = [...result, newScore];
        break;
      case "D":
        newScore = result[si] * 2; //double of previous score
        result = [...result, newScore];
        break;
      case "C":
        result.pop(); //invalidate and remove previous score
        break;
      default: //number score, parse to int
        newScore = +op;
        result = [...result, newScore];
        break;
    }
  }
  //sum up the scores
  result = result.reduce((a, b) => a + b, 0);
  return result;
};

const opsList1 = ["5", "2", "C", "D", "+"];
console.log(`Input ${opsList1.join(" ")} = ${calPoints(opsList1)}`);
//expected output: 30

const opsList2 = ["5", "-2", "4", "C", "D", "9", "+", "+"];
console.log(`Input ${opsList2.join(" ")} = ${calPoints(opsList2)}`);
//expected output: 27

const opsList3 = ["1"];
console.log(`Input ${opsList3.join(" ")} = ${calPoints(opsList3)}`);
//expected output: 1
