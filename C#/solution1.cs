using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

class Solution{
    public int CallPoints(string[] ops) {
        int points = 0;
        int last = 0;
        for (int i = 0; i < ops.Length; i++) {
            if (ops[i] == "C") {
                points -= last;
                last = 0;
            } else if (ops[i] == "+") {
                points += last;
            } else {
                last = int.Parse(ops[i]);
            }
        }
        return points;
    } 
}
