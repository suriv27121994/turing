function find_max(nums) {
  let max_num = Number.NEGATIVE_INFINITY; // smaller than all other numbers
  for (let num of nums) {
    if (num > max_num) {
      // (Fill in the missing line here)
      max_num = num; // can = 5
      //num = max_num; // -Infinity
      //max_num += 1;  // -Infinity
      // max_num += num; // -Infinity
    }
  }
  return max_num;
}

console.log(find_max([1, 2, 3, 4, 5]));
